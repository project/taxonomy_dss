<?php

/**
 * @file
 * The taxonomy_dss_newcontent module publishes new content from all
 * sub-sections in a taxonomy's section as part of the taxonomy page's output.
 */

define('TAXONOMY_DSS_NEWCONTENT_DEFAULT_HIDDEN', 1);
define('TAXONOMY_DSS_NEWCONTENT_DEFAULT_FROM_ANYWHERE', 0);
define('TAXONOMY_DSS_NEWCONTENT_DEFAULT_COUNT', 6);

/**
 * Implementation of hook_form_alter().
 */
function taxonomy_dss_newcontent_form_alter($form_id, &$form) {
  switch ($form_id) {
    case 'taxonomy_dss_admin_settings':
      $form['newcontent_options'] = array(
        '#type' => 'fieldset',
        '#title' => t('Default new content listing options'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#weight' => -3,
      );
      $form['newcontent_options']['taxonomy_dss_newcontent_hidden'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show new content'),
        '#default_value' => !variable_get('taxonomy_dss_newcontent_hidden', TAXONOMY_DSS_NEWCONTENT_DEFAULT_HIDDEN),
        '#description' => t('List new content from this term\'s sub-categories.'),
      );
      $form['newcontent_options']['taxonomy_dss_newcontent_from_anywhere'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use any new content'),
        '#default_value' => variable_get('taxonomy_dss_newcontent_from_anywhere', TAXONOMY_DSS_NEWCONTENT_DEFAULT_FROM_ANYWHERE),
        '#description' => t('New content can come from any term. Essentially list any new content on the site.'),
      );
      $form['newcontent_options']['taxonomy_dss_newcontent_count'] = array(
        '#type' => 'textfield',
        '#title' => t('Amount of new nodes'),
        '#default_value' => variable_get('taxonomy_dss_newcontent_count', TAXONOMY_DSS_NEWCONTENT_DEFAULT_COUNT),
        '#description' => t('The amount of new nodes to list from this term\'s sub-categories.'),
      );
      $form['newcontent_options']['taxonomy_dss_newcontent_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => variable_get('taxonomy_dss_newcontent_title', t('New Content')),
        '#description' => t('This title appears at the top of the new content section. Leave blank for no title.'),
      );
      break;

    case 'term_node_form':
      if (arg(0) == 'taxonomy' && arg(1) == 'term' && (arg(3) == 'add' || arg(3) == 'edit')) {

        $termset = taxonomy_dss_load(arg(2));

        $form['newcontent_options'] = array(
          '#type' => 'fieldset',
          '#title' => t('New content listing options'),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
          '#weight' => -3,
        );
        $form['newcontent_options']['newcontent_hidden'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show new content'),
          '#default_value' => isset($node->newcontent_hidden) ? !$node->newcontent_hidden : !$termset->serial_data['newcontent_hidden'],
          '#description' => t('List new content from this term\'s sub-categories.'),
        );
        $form['newcontent_options']['newcontent_from_anywhere'] = array(
          '#type' => 'checkbox',
          '#title' => t('Use any new content'),
          '#default_value' => isset($node->newcontent_from_anywhere) ? $node->newcontent_from_anywhere : $termset->serial_data['newcontent_from_anywhere'],
          '#description' => t('New content can come from any term. Essentially list any new content on the site.'),
        );
        $form['newcontent_options']['newcontent_count'] = array(
          '#type' => 'textfield',
          '#title' => t('Amount of new nodes'),
          '#default_value' => isset($node->newcontent_count) ? $node->newcontent_count : $termset->serial_data['newcontent_count'],
          '#description' => t('The amount of new nodes to list from this term\'s sub-categories.'),
        );
        $form['newcontent_options']['newcontent_title'] = array(
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#default_value' => isset($node->newcontent_title) ? $node->newcontent_title : $termset->serial_data['newcontent_title'],
          '#description' => t('This title appears at the top of the new content section. Leave blank for no title.'),
        );

      }
      break;
  }
}

/**
 * Implementation of hook_taxonomy_dss().
 *
 * $term
 *      ->terms
 *      ->content
 */
function taxonomy_dss_newcontent_taxonomy_dss(&$termset, $op, $node = NULL) {
  switch ($op) {
    case 'load':
      // Set defaults
      if (!isset($termset->serial_data['newcontent_hidden'])) {
        $termset->serial_data['newcontent_hidden'] = variable_get('taxonomy_dss_newcontent_hidden', TAXONOMY_DSS_NEWCONTENT_DEFAULT_HIDDEN);
      }
      if (!isset($termset->serial_data['newcontent_from_anywhere'])) {
        $termset->serial_data['newcontent_from_anywhere'] = variable_get('taxonomy_dss_newcontent_from_anywhere', TAXONOMY_DSS_NEWCONTENT_DEFAULT_FROM_ANYWHERE);
      }
      if (!isset($termset->serial_data['newcontent_count'])) {
        $termset->serial_data['newcontent_count'] = variable_get('taxonomy_dss_newcontent_count', TAXONOMY_DSS_NEWCONTENT_DEFAULT_COUNT);
      }
      if (!isset($termset->serial_data['newcontent_title'])) {
        $termset->serial_data['newcontent_title'] = variable_get('taxonomy_dss_newcontent_title', t('New Content'));
      }
      break;

    case 'submit':
      $termset->serial_data['newcontent_hidden'] = !$node->newcontent_hidden;
      $termset->serial_data['newcontent_from_anywhere'] = $node->newcontent_from_anywhere;
      $termset->serial_data['newcontent_count'] = $node->newcontent_count;
      $termset->serial_data['newcontent_title'] = $node->newcontent_title;

      break;

    case 'page':
      if (!$termset->serial_data['newcontent_hidden']) { // need to call variable that manages showing newcontent on a term page
        if ($termset->serial_data['newcontent_from_anywhere']) {
          $result = pager_query(db_rewrite_sql('SELECT n.nid, n.sticky, n.created FROM {node} n WHERE n.promote = 1 AND n.status = 1 ORDER BY n.sticky DESC, n.created DESC'), $termset->serial_data['newcontent_count']);
          $newcontent = _taxonomy_dss_get_tree_nodes($termset->terms['tids'], $result);
        }
        else {
          $result = taxonomy_dss_select_nodes($termset->terms['tids'], $termset->terms['operator'], 100, FALSE, 'n.created DESC', FALSE, TRUE, $termset->serial_data['newcontent_count']);
          $newcontent = _taxonomy_dss_get_tree_nodes($termset->terms['tids'], $result);
        }

        $termset->content['newcontent'] = array(
          '#value' => theme('taxonomy_dss_newcontent', $termset, $newcontent),
          '#weight' => 1,
        );

        return $termset;
      }
      break;

    case 'feed':
      return $termset;
      break;
  }
}

/**
 * Theme for displaying new content from the taxonomy term's sub-terms.
 *
 * @ingroup themeable
 */
function theme_taxonomy_dss_newcontent(&$termset, &$nodes) {
  $ouput = '';
  $output .= '<div class="taxonomy-dss-newcontent">';
  $output .= $termset->serial_data['newcontent_title'] ? '<h3>' . $termset->serial_data['newcontent_title'] . '</h3>' : '';

  foreach ($nodes as $node) {
    $output .= node_view($node, TRUE, FALSE, TRUE);
  }

  $output .= '</div>';

  return $output;
}
